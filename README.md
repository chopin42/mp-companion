# DISCONTINUED: Can contain large security breaches.

This project isn't finished but I found out another way to use it using the Userscripts that will later be converted into extension and add-ons. The new project is called [master-browser](https://gitea.com/chopin42/master-browser)

I actually don't even remmember have doing this project so I don't even know if it works. You can try to install it but there is a very high chance that it don't work. This is a Chrome only extension, while [master-browser](https://gitea.com/chopin42/master-browser) is cross-platform.

# Masterpassword Companion

Get masterpassword app everywhere online directly in the context menu. This is an extension for Chrome.

## Install

You can install the beta version by

1. Allow the Devlopper option in your extensions settings
2. Load an unpacked app
3. Select this directory

## Contributing

If you want to contribute, you can do several things :

- Go into the issue to report somehting or propose ideas
- Solve other's issues technical or documentation
- Build the documentation of the app
- Make changes to solve issues, yes I am newbie in Java Script, thanks for your help.

## License

This projec thas been made by SnowCode and is under the GNU-GPLV3 open-source license.
